import React from 'react';

import "./QuoteIndividual.css";

const QuoteIndividual = ({author, text, editHandler, removeHandler}) => {
  return (
      <div className="QuoteBlock">
        <div className="QuoteBox">
          <p className="QuoteText">"{text}"</p>
          <p className="QuoteAuthor">— {author}</p>
        </div>
        <div className="QuoteInteraction">
          <button className="QuoteBtn QuoteBtn_edit" onClick={editHandler}>&#9998;</button>
          <button className="QuoteBtn QuoteBtn_remove" onClick={removeHandler}>&times;</button>
        </div>
      </div>
  );
};

export default QuoteIndividual;