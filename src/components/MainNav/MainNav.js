import React from 'react';
import {Link, NavLink} from "react-router-dom";

import "./MainNav.css";

const MainNav = () => {
  return (
      <div className="MainNavBlock">
        <div className="container">
          <div className="MainNav">
            <Link className="Logo" to="/">
              <h2>Quotes Central</h2>
            </Link>
            <div className="NavMenu">
              <NavLink className="NavMenuItem" exact to="/">Quotes</NavLink>
              <NavLink className="NavMenuItem" to="/add_quote">Submit new quote</NavLink>
            </div>
          </div>
        </div>
      </div>
  );
};

export default MainNav;