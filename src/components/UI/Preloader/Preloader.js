import React from 'react';
import SpinnerSVG from "./482.svg";

import "./Preloader.css";

const Preloader = () => {
  return (
      <div className="Backdrop">
        <div className="PreloaderBack">
          <img className="PreloaderImg" src={SpinnerSVG} alt=""/>
        </div>
      </div>
  );
};

export default Preloader;