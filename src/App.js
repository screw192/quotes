import React from 'react';
import {Switch, Route} from "react-router-dom";

import MainNav from "./components/MainNav/MainNav";
import AddQuote from "./containers/AddQuote/AddQuote";
import Quotes from "./containers/Quotes/Quotes";
import './App.css';

const App = () => {
  return (
    <div className="App">
      <MainNav />
      <Switch>
        <Route exact path="/" component={Quotes} />
        <Route exact path="/quotes/:id" component={Quotes} />
        <Route path="/quotes/:id/edit" component={AddQuote} />
        <Route exact path="/add_quote" component={AddQuote} />
      </Switch>
    </div>
  );
}

export default App;
