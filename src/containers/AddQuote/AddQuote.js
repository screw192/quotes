import React, {useState, useEffect} from 'react';
import axiosOrders from "../../axios-orders";
import QUOTE_MENU from "../../quote-categories";

import Preloader from "../../components/UI/Preloader/Preloader";
import "./AddQuote.css";

const AddQuote = props => {
  const [quote, setQuote] = useState({
    quoteAuthor: "",
    quoteCategory: "",
    quoteText: ""
  });
  const [beingEdited, setBeingEdited] = useState(false);
  const [disabled, setDisabled] = useState(true);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (props.match.params.id) {
      setBeingEdited(true);
    }
  }, [props.match.params.id])

  useEffect(() => {
    if (quote.quoteAuthor !== "" && quote.quoteCategory !== "" && quote.quoteText !== "") {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  }, [quote]);

  useEffect(() => {
    if (props.match.params.id) {
      const fetchData = async () => {
        setLoading(true);
        const response = await axiosOrders.get("quotes/" + props.match.params.id + ".json");

        const quoteToEdit = {
          quoteAuthor: response.data.quoteAuthor,
          quoteCategory: response.data.quoteCategory,
          quoteText: response.data.quoteText
        };
        setQuote(quoteToEdit);

        setLoading(false);
      };

      fetchData().catch(console.error);
    } else {
      setQuote({
        quoteAuthor: "",
        quoteCategory: "",
        quoteText: ""
      })
    }
  }, [props.match.params.id]);

  const submitHandler = async event => {
    event.preventDefault();
    setLoading(true);

    try {
      if (props.match.params.id) {
        await axiosOrders.put("quotes/" + props.match.params.id + ".json", quote);
      } else {
        await axiosOrders.post("quotes.json", quote);
      }
    } finally {
      setLoading(false);
      props.history.push("/");
    }
  };

  const inputHandler = event => {
    const {name, value} = event.target;

    setQuote(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  let preloader = null;
  let title = null;

  const options = QUOTE_MENU.map(item => {
    return (
        <option
            key={item.id}
            value={item.id}
        >{item.title}</option>
    );
  });

  if (loading) {
    preloader = <Preloader />;
  }

  beingEdited ? title = "Edit a quote" : title = "Submit new quote";

  return (
      <>
        {preloader}
        <div className="AddQuoteBlock">
          <div className="container">
            <div className="AddQuote">
              <h4 className="AddQuoteTitle">{title}</h4>
              <form onSubmit={submitHandler}>
                <div className="FormRow">
                  <label htmlFor="quoteCategory">Category:</label>
                  <select
                      id="quoteCategory"
                      name="quoteCategory"
                      value={quote.quoteCategory}
                      onChange={inputHandler}
                  >
                    <option value="" disabled hidden>Choose category...</option>
                    {options}
                  </select>
                </div>
                <div className="FormRow">
                  <label htmlFor="quoteAuthor">Author:</label>
                  <input id="quoteAuthor"
                         type="text" name="quoteAuthor"
                         value={quote.quoteAuthor}
                         onChange={inputHandler}
                  />
                </div>
                <div className="FormRow">
                  <label htmlFor="quoteText">Quote text:</label>
                  <textarea
                      id="quoteText"
                      name="quoteText"
                      value={quote.quoteText}
                      onChange={inputHandler}
                      style={{resize: "none"}}
                  />
                </div>
                <button disabled={disabled} className="SaveQuoteButton">Save</button>
              </form>
            </div>
          </div>
        </div>
      </>
  );
};

export default AddQuote;