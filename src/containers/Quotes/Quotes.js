import React, {useState, useEffect} from 'react';
import axiosOrders from "../../axios-orders";
import {Link} from "react-router-dom";
import QUOTE_MENU from "../../quote-categories";

import QuoteIndividual from "../../components/QuoteIndividual/QuoteIndividual";
import Preloader from "../../components/UI/Preloader/Preloader";
import "./Quotes.css";

const Quotes = props => {
  const [quotes, setQuotes] = useState({});
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      let catUrl = "";
      const headUrl = '?orderBy="quoteCategory"&equalTo=';

      switch (props.match.params.id) {
        case undefined:
          break;
        case QUOTE_MENU[0].id:
          catUrl = `${headUrl}"${QUOTE_MENU[0].id}"`;
          break;
        case QUOTE_MENU[1].id:
          catUrl = `${headUrl}"${QUOTE_MENU[1].id}"`;
          break;
        case QUOTE_MENU[2].id:
          catUrl = `${headUrl}"${QUOTE_MENU[2].id}"`;
          break;
        case QUOTE_MENU[3].id:
          catUrl = `${headUrl}"${QUOTE_MENU[3].id}"`;
          break;
        case QUOTE_MENU[4].id:
          catUrl = `${headUrl}"${QUOTE_MENU[4].id}"`;
          break;
        default:
          break;
      }
      const response = await axiosOrders.get("quotes.json" + catUrl);

      setQuotes(response.data);
      setLoading(false);
    };

    fetchData().catch(console.error);
  }, [props.match.params.id]);

  const editPostHandler = id => {
    props.history.replace("/"); //костыль ----------------------------------------------------
    props.history.push("quotes/" + id + "/edit");
  }

  const removeQuoteHandler = async id => {
    setLoading(true);
    await axiosOrders.delete("quotes/" + id + ".json");
    setLoading(false);

    window.location.reload();
  };

  let quotesBlock = [];
  let preloader = null;

  const quoteCategories = QUOTE_MENU.map(item => {
    return (
        <Link
            key={item.id}
            to={"/quotes/" + item.id}
            className="CatMenuItem"
        >{item.title}</Link>
    );
  });

  for (let key in quotes) {
    quotesBlock.unshift(
        <QuoteIndividual
            key={key}
            author={quotes[key].quoteAuthor}
            text={quotes[key].quoteText}
            editHandler={() => editPostHandler(key)}
            removeHandler={() => removeQuoteHandler(key)}
        />
    );
  }

  if (loading) {
    preloader = <Preloader />;
  }

  return (
      <>
        {preloader}
        <div className="MainBlock">
          <div className="container">
            <div className="MainBox">
              <div className="CategoriesBlock">
                <Link to="/" className="CatMenuItem">All</Link>
                {quoteCategories}
              </div>
              <div className="QuotesBlock">
                {quotesBlock}
              </div>
            </div>
          </div>
        </div>
      </>
  );
};

export default Quotes;