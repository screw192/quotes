import axios from "axios";

const axiosOrders = axios.create({
  baseURL: "https://js9-exam8-quotes-default-rtdb.firebaseio.com/"
});

export default axiosOrders;